new WOW().init();

$(document).ready(()=>{
    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        rtl: true,
        lazyLoad: true,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 5
            }
        }
    });
    lazyload();

    $.ajax('https://cp.dev-any.com/api/v1/projects/')
    .done(resp=>{
        if (resp.success) {
            let data = resp.data;
            for (let i = 0; i < data.length; i++) {
                const element = data[i];
                // console.log(element.title);
                $('#projectsList').append(`
                    <li class="media wow fadeInUp">
                        <a data-fancybox="gallery1" class="d-flex" href="` + element.images[0].url + `">
                            <img class="lazyload media-image"
                                src=""
                                data-src= "` + element.images[0].url + `"
                                alt= "` + element.images[0].title + `" >
                        </a>
                        <div class="media-body">
                            <h6>` + element.title + `</h6>
                            <p>` + element.slug + `</p>
                        </div>
                    </li>
                `);
            }
            lazyload();
        }
    })
    .fail(err=>console.error(err));
    
    var i = 0;
    var inter = setInterval(() => {
        console.log(i++);
        if(i> 4){
            clearInterval(inter);
        }
    }, 1000);

    setTimeout(() => {
        console.log('timeout is run reload');
        // window.location.reload()
    }, 6000);

    console.log(window.location.host);
    

});