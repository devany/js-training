$(document).ready(()=>{

    $('#login').on('submit', e =>{
        e.preventDefault();
        var email = $('#email').val();
        var password = $('#password').val();
        $.ajax("https://products-api.devany.co/public/api/login",{
            method: "POST",
            data: {
                email: email,
                password: password
            }
        })
        .done(resp => {
            console.log(resp);
        })
        .fail(err => {
            console.error("Email and/or password is incorrect!");
        })
        .always(() => {
            // alert("complete");
        });
    });

});