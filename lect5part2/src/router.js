import VueRouter from 'vue-router'
import Home from './components/HelloWorld.vue';
import About from './components/about.vue';

const routes = [{
        path: '/',
        component: Home
    },
    {
        path: '/about',
        component: About
    }
];
export default new VueRouter({
    routes  
});